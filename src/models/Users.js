const mongoose = require('mongoose');
const Joi = require('joi');

// const userJoiSchema = Joi.object({
//   username: Joi.string()
//     .alphanum()
//     .min(2)
//     .max(30)
//     .required(),

//   password: Joi.string()
//     .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

//   name: Joi.string()
//     .alphanum()
//     .min(2)
//     .max(60)
// });

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    // default: ['shipper'],
    required: true,
  },
  createdDate: {
    type: String,
    default: true
  }
});

module.exports = {
  User,
  //userJoiSchema
};
