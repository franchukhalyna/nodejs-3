const { User } = require('../models/Users');
const bcrypt = require('bcryptjs');
//const jwt = require('jsonwebtoken');


const getUsersProfile = (req, res, next) => {
  User.findById(req.user.userId, function(err, user) {
    if (err) {
      res.status(200).json({'message': 'User not found'});
    } else {
      const result = {"_id": req.user.userId, "role": user.role, "email": user.email, "created_date": user.createdDate };
      res.status(200).json({"user": result});
    }
  })
} 

const deleteUsersProfile = (req, res, next) => {
  User.findByIdAndDelete(req.user.userId, function(err, user) {
    if (err || !user) {
      res.status(200).json({'message': 'User not found'});
    } else {
      res.status(200).json({"message": "success. user removed."});
    }
  })
}

const updateUsersPassword =  async (req, res, next) => {
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    const { oldPassword, newPassword } = req.body;
    console.log(newPassword);
    const newUserPassword = await bcrypt.hash(newPassword, 10);
    User.findByIdAndUpdate({_id: req.user.userId, userId: req.user.userId}, {$set: { password: newUserPassword } }, function(err) {
      if(err) {
        res.status(400).json({'message': 'Error'});
      } else {
        res.status(200).json({'message': 'Password was updated'});
      }
    })
  }
}



module.exports = {
  getUsersProfile,
  deleteUsersProfile,
  updateUsersPassword
};