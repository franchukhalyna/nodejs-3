const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');

const app = express();

//mongoose.connect('mongodb+srv://admin-galina:12345@cluster0.rfa6e.mongodb.net/notesapp?retryWrites=true&w=majority');

// app.use(express.static('public'));

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});
app.use(morgan('tiny', {stream: accessLogStream}));

app.use(cors());
app.use(express.json());

const { auth } = require('./routes/auth');
const { users } = require('./routes/users');
const { loads } = require('./routes/loads');
const { trucks } = require('./routes/trucks');

app.use('/api/auth', auth);
app.use('/api/users/me', users);
app.use('/api/loads', loads);
app.use('/api/trucks', trucks);


const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://admin-galina:13579@cluster0.rfa6e.mongodb.net/delivery?retryWrites=true&w=majority');
    app.listen(8080, () => console.log('Server started on port 8080'));
  } catch(err) {
    console.log('Server error', err.message);
    process.exit(1);
  }
};
start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
