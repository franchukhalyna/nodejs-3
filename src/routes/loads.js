const express = require('express');
const router = express.Router();


const { createUserLoad, postUserLoad, getUsersLoadsById, iterateLoadState, deleteUserLoad, updateUserLoadById, getUserActiveLoad } = require('../services/loads');
const { middleware } = require('../middleware/middleware');


router.post('/', middleware, createUserLoad);

router.post('/:id?/post', middleware, postUserLoad);

router.get('/active', middleware, getUserActiveLoad);

router.get('/:id', middleware, getUsersLoadsById);

router.patch('/active/state', middleware, iterateLoadState);

router.put('/:id?', middleware, updateUserLoadById);

router.delete('/:id?', middleware, deleteUserLoad);

module.exports = {
  loads: router,
};