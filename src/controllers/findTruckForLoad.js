
const truck = {
  'SPRINTER': {
    payload: 1700,
    dimension: {
      width: 170,
      height: 250,
      length: 300
    }
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    dimension: {
      width: 170,
      height: 250,
      length: 500
    }
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    dimension: {
      width: 200,
      height: 350,
      length: 700
    }
  }
}


const findTruck = function(payload, dimensions) {
  let resultArr = [];
  for(key in truck) {
    if(checkSuitable(key, payload, dimensions)) {
      resultArr.push(key);
    }
  }
  return resultArr;
}

function checkSuitable(type, payload, dimensions) {
  if(truck[type].payload>=payload && truck[type].dimension.width>=dimensions.width &&
   truck[type].dimension.length>=dimensions.length && 
   truck[type].dimension.height>=dimensions.height) {
    return true;
   }
}

module.exports = {
  findTruck
}