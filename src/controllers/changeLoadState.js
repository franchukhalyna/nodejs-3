const { Load } = require('../models/Loads')

const changeLoadState = function(currentState) {
  //console.log(currentState);
  const enumValues = Load.schema.path('state').enumValues;
  const index = enumValues.indexOf(currentState);
  if(index===enumValues.length-1) {
    return enumValues[index];
  } 
  return enumValues[index+1];
}

module.exports = {
  changeLoadState,
}