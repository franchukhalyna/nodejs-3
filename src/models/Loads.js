const mongoose = require('mongoose');

const loadsSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  assigned_to: {
    type: String,
    default: null
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPER'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up',
           'En route to delivery', 'Arrived to delivery'],
    //default: null
  },
  name: {
    type: String,
    default: 'New load'
  },
  payload: {
    type: Number,
    default: 0
  },
  pickup_address: {
    type: String,
    default: null
  },
  delivery_address: {
    type: String,
    default: null
  },
  dimensions: {
    width: {
      type: Number,
      default: 0
    },
    length: {
      type: Number,
      default: 0
    },
    height: {
      type: Number,
      default: 0
    },
  },
  logs: [{
    message: {type: String, default: null},
    time: {type: String, default: null}
  }],
  created_date: {
    type: String,
    default: null
  }
});

const Load = mongoose.model('load', loadsSchema);

module.exports = {
  Load
};
