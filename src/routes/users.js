const express = require('express');
const router = express.Router();


const { getUsersProfile, deleteUsersProfile, updateUsersPassword } = require('../services/users');
const { middleware } = require('../middleware/middleware');


router.get('/', middleware, getUsersProfile);

router.patch('/password', middleware, updateUsersPassword);

router.delete('/', middleware, deleteUsersProfile);


module.exports = {
  users: router,
};
