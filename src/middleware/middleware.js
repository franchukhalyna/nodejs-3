const jwt = require('jsonwebtoken');

const middleware = (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }
  const [, token] = authorization.split(' ');
  //const token = authorization;

  if (!token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    req.user = {
      userId: tokenPayload.userId,
      email: tokenPayload.email,
      role: tokenPayload.role
    }
    next();
  } catch (err) {
    return res.status(401).json({"Err message": err.message});
  }

}

module.exports = {
  middleware
}
