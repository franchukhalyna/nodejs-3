const express = require('express');
const router = express.Router();

const { createUserTruck, getUsersTrucks, getUsersTruckById, updateUsersTruckById, deleteUsersTruckById, assignTruckById } = require('../services/truck');
const { middleware } = require('../middleware/middleware');


router.post('/', middleware, createUserTruck);

router.get('/', middleware, getUsersTrucks);

router.get('/:id', middleware, getUsersTruckById);

router.put('/:id', middleware, updateUsersTruckById);

router.delete('/:id', middleware, deleteUsersTruckById);

router.post('/:id/assign', middleware, assignTruckById)



module.exports = {
  trucks: router,
};
