const { User } = require('../models/Users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const registerUser = async (req, res, next) => {
  if(Object.keys(req.body).length===0 || !req.body.email || !req.body.password || !req.body.role) {
    res.status(400).send({ "message": "Please specify right parameters" });
  } else {
    const { email, password, role } = req.body;
    const createdDate = JSON.stringify(new Date()).replace(/['"]+/g, '');
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
      createdDate
    });

    user.save()
      .then(saved => {
        res.status(200).json({'message': "Success"})
      })
      .catch(err => {
        next(err);
      });
  }
}

const loginUser = async (req, res, next) => {
  if(!req.body.email || !req.body.password) {
    return res.status(200).json({'message': 'Wrong param'});
  } else {
    const user = await User.findOne({ email: req.body.email });
    if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res
    .cookie("access_token", jwtToken)
    .status(200)
    .json({ "message": "Success", jwt_token: jwtToken});
    } else if(!user) {
      return res.status(400).json({'message': 'User not find'});
    }  
  }
  return res.status(400).json({'message': 'Err'});
}


const getUserPassword = async (req, res, next) => {
  res.status(200).json({"message": "password"});
}

module.exports = {
  registerUser,
  loginUser,
  getUserPassword
};
