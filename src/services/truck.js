const { Truck } = require('../models/Trucks');
const { User } = require('../models/Users');

function createUserTruck(req, res, next) {
  
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    try {
      const { type } = req.body;
      const { role } = req.user;
      if(!type) {
        res.status(400).json({'message': 'Type undefined'});
      } else if(!role || role!=='DRIVER') {
        res.status(400).json({'message': 'Only driver can create trucks'});
      } else {
        const created_by = req.user.userId;
        const created_date = JSON.stringify(new Date()).replace(/['"]+/g, '');
        if(!Truck.schema.path('type').enumValues.includes(type)) {
        res.status(400).json({'message': 'Invalid type'});
      } else {
        const truck = new Truck({
          created_by,
          type,
          created_date
        });
        truck.save().then((saved) => {
          res.status(200).json({'message': 'Truck created successfully'});
        });
      }
      }
    } 
    catch(e) {
      res.status(400).json({'message': 'Error'});
    }    
  } 
}

const getUsersTrucks = async (req, res, next) => {
  try {
    const { role } = req.user;
    if(!role || role!=='DRIVER') {
      res.status(400).json({'message': 'Only driver can get trucks'});
    } else {
      const agg = [
        {
          '$match': {
              'email': req.user.email
          }
        }, {
          '$lookup': {
              'from': 'trucks', 
              'localField': '_id', 
              'foreignField': 'created_by', 
              'as': 'trucks'
          }
        }
      ];
      const usersTrucks = await User.aggregate(agg);
      res.status(200).json({'trucks': usersTrucks[0].trucks});  
    }
  }  
  catch(err) {
    res.status(400).json({'message': 'Error'});
  }
}

const getUsersTruckById = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can get truck by id'});
  } else {
    Truck.findById(req.params.id, function(err, truck) {
      if (err || !truck) {
        res.status(200).json({"truck": {'message': 'Id not found'}});
      } else {
        res.status(200).json({"truck": truck});
      }
    });
  }
}

const updateUsersTruckById = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can update trucks'});
  } else {
    const { type } = req.body;
    if(Object.keys(req.body).length===0 || !Truck.schema.path('type').enumValues.includes(type)) {
      res.status(400).json({'message': 'Invalid parameters'});
    } else {
      return Truck.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { type } }, function(err) {
        if(err) {
          res.status(400).json({'message': 'Error'});
        } else {
          res.status(200).json({'message': 'Truck info was updated'});
        }
      })
    }
  }
}

const deleteUsersTruckById = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can update trucks'});
  } else {
    Truck.findByIdAndDelete(req.params.id, function(err, truck) {
      if (err || !truck) {
        res.status(400).json({'message': 'Id not found'});
      } else {
        res.status(200).json({'message': 'Truck deleted successfully'});
      }
    });
  }
}


const assignTruckById = async(req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can assign trucks'});
  } else {
    const assigned_to = req.user.userId;
    //console.log(Truck.find({ assigned_to: {assigned_to} }));
    const agg = 
      [
        {
            '$match': {
                'assigned_to': assigned_to
            }
        }
      ]
    //);
    const checkDriverHasTruck = await Truck.aggregate(agg);
    if(checkDriverHasTruck.length===1) {
      res.status(400).json({'message': 'Driver has a truck'});
    } else {
      Truck.findByIdAndUpdate({_id: req.params.id}, {$set: { assigned_to } }, function(err, truck) {
        if (err || !truck) {
          res.status(400).json({'message': 'Id not found'});
        } else {
          //console.log(truck);
          res.status(200).json({'message': 'Truck was assigned successfully'});
        }
      });
    }
  }
}

module.exports = {
  createUserTruck,
  getUsersTrucks,
  getUsersTruckById,
  updateUsersTruckById,
  deleteUsersTruckById,
  assignTruckById
};