const express = require('express');
const router = express.Router();
const { registerUser, loginUser, getUserPassword } = require('../services/authService.js');
const { authMiddleware } = require('../middleware/authMiddleware.js');


router.post('/register', registerUser);

router.post('/login', loginUser);

router.post('/forgot_password', getUserPassword);

module.exports = {
  authRoutes: router,
};