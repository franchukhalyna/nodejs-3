const mongoose = require('mongoose');
const { Truck } = require('../models/Trucks');
const { User } = require('../models/Users');
const { Load } = require('../models/Loads');
const { findTruck } = require('../controllers/findTruckForLoad');
const { changeLoadState } = require('../controllers/changeLoadState');
const e = require('express');

const createUserLoad = (req, res, next) => {  
  if(Object.keys(req.body).length===0) {
    res.status(400).json({'message': 'Invalid parameters'});
  } else {
    try {
      const { name, payload, pickup_address, delivery_address, dimensions: {width, length, height} } = req.body;
      const { role, userId } = req.user;
      if(!role || role!=='SHIPPER') {
        res.status(400).json({'message': 'Only shipper can create loads'});
      } else {
        const created_by = userId;
        const created_date = JSON.stringify(new Date()).replace(/['"]+/g, '');
        // if(!Load.schema.path('type').enumValues.includes(type)) {
        // res.status(400).json({'message': 'Invalid type'});
        // } else {
          const load = new Load({
            created_by,
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions: {width, length, height},
            created_date
          });
          load.save().then((saved) => {
            res.status(200).json({'message': 'Load created successfully'});
          });
       // }
      }
    } 
    catch(e) {
      res.status(400).json({'message': 'Error load'});
    }    
  } 
}

const postUserLoad = async (req, res, next) => {
  const { role } = req.user;
  let userLoad = new Object();
  let suitTruck = [];
  if(!role || role!=='SHIPPER') {
    res.status(400).json({'message': 'Only shipper can post loads'});
  } else {
    try{ 
      userLoad =  await Load.findByIdAndUpdate(req.params.id, {
        $set: {
            status: 'POSTED'
        }
    });
    if(!userLoad) {
      throw e;
    } 
    } catch(e) {
      //console.log(e);
      return res.status(200).json({'message': 'Id not found', 'driver_found': true});
    }
    const {payload, dimensions} = userLoad;
    const truckType = findTruck(payload, dimensions);
    try {
      suitTruck = await Truck.find(
        {type: { $in: truckType}, assigned_to: {$ne:null}, status: 'IS' });
        
      if(suitTruck.length===0 || !suitTruck) {
        Load.findByIdAndUpdate(
          {_id: req.params.id}, 
          {$set: {status: 'NEW', state: 'New',}, 
          $push: {logs: {message: 'new', time: true}}},
          function(err) {
            if(err) {
              console.log(err);
              return res.status(200).json({'message': 'Truck not found'});
              //res.status(400).json({'message': 'Error'});
            }
          });
        } else {
          const suitTruckId = suitTruck[0]._id.valueOf();
          Truck.findByIdAndUpdate({_id: suitTruckId}, {$set: {status: "OL"}}, function(err) {
          if(err) {
            res.status(400).json({'message': 'Error'});
          }
          });
          Load.findByIdAndUpdate({_id: req.params.id}, 
          {$set: {status: 'ASSIGNED', assigned_to: suitTruckId, state: "En route to Pick Up" },
          $push: {'logs': {'message': 'En route to Pick Up', 'time': true}}}, 
          function(err) {
          if (err) {
            res.status(400).json({'message': 'Error'});
          } else {
            res.status(200).json({'message': 'Load posted successfully', "driver_found": true});
          }
          });
        }
    } catch(e) {
      console.log(e);
    }
    //console.log(suitTruck);
    
  }
}

const getUsersLoadsById = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='SHIPPER') {
    res.status(200).json({'message': 'Only shipper can get his loads'});
  } else {
    Load.find({_id: req.params.id, created_by: req.user.userId}, function(err, load) {
      if (err) {
        res.status(400).json({'message': 'Error. Wrong param.'});
      } else {
        res.status(200).json({"loads": load});
      }
    });
  }
}

const iterateLoadState = async (req, res, next) => {
  const {userId, role } = req.user;
  if(role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can change state'});
  } else {
    try {
      const assignedTruck = await Truck.find({assigned_to: userId});
      const truckId = assignedTruck[0]._id.valueOf();
      const assignedLoad = await Load.find({assigned_to: truckId});
      const newLoadState = changeLoadState(assignedLoad[0].state);
      await Load.findOneAndUpdate({assigned_to: truckId},
        {$set: {state: newLoadState},
        $push: {'logs': {'message': newLoadState, 'time': true}}
      });
      if(newLoadState==='Arrived to delivery') {
        await Load.findOneAndUpdate({assigned_to: truckId}, {$set: {status: 'SHIPPED'}});
        await Truck.findByIdAndUpdate({_id: truckId}, {$set: {status: 'IS'}});
      }
    } catch(err) {
      console.log(err);
    }
    res.status(200).json({'message': 'success'});
  }
}

const updateUserLoadById = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='SHIPPER') {
    res.status(200).json({'message': 'Only shipper can update loads'});
  } else {
    const { name, payload, pickup_address, delivery_address, dimensions: {width, length, height} } = req.body;
    Load.findByIdAndUpdate({_id: req.params.id, created_by: req.user.userId}, 
      {$set: {name, payload, pickup_address, delivery_address, dimensions: {width, length, height}}}, function(err, load) {
          if (err) {
            res.status(400).json({'message': 'Error. Wrong param or id'});
          } else {
            res.status(200).json({'message': 'Load details changed successfully'});
          }
      });
  }
}


const deleteUserLoad = (req, res, next) => {
  const { role } = req.user;
  if(!role || role!=='SHIPPER') {
    res.status(400).json({'message': 'Only shipper can delete loads'});
  } else {
    Load.findByIdAndDelete(req.params.id, function(err, load) {
      if (err) {
        res.status(400).json({'message': 'Id not found'});
      } else {
        res.status(200).json({'message': 'Load was deleted successfully'});
      }
    });
  }
}


const getUserActiveLoad = async (req, res, next) => {
  const { role, userId } = req.user;
  if(!role || role!=='DRIVER') {
    res.status(400).json({'message': 'Only driver can get his loads'});
  } else {
    const agg = [
      {
          '$match': {
              '_id': mongoose.Types.ObjectId(userId)
          }
      },
      {
        '$lookup': {
            'from': 'trucks', 
            'localField': '_id', 
            'foreignField': 'created_by', 
            'as': 'truck'
        }
      }
    ];
    const usersTruck = await User.aggregate(agg);
    const truckId = usersTruck[0].truck[0]._id.valueOf();
    if(usersTruck.length!==0) {
      Load.find({assigned_to: truckId, state: 'En route to Pick Up'}, function(err, load) {
        if(err) {
          console.log(err);
          return res.status(400).json({'message': 'err'});
        } else if(load) {
          return res.status(200).json({'load': load[0]});
        }
        return res.status(200).json({'load': 'Not found'});
      });
    }
  }
}




module.exports = {
  createUserLoad,
  postUserLoad,
  getUsersLoadsById,
  iterateLoadState,
  deleteUserLoad,
  updateUserLoadById,
  getUserActiveLoad,
}