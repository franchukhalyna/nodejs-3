const mongoose = require('mongoose');

const trucksSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  // created_by: {
  //   type: String,
  //   required: true,
  // },
  assigned_to: {
    type: String,
    default: null
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },
  created_date: {
    type: String,
    default: true
  }
});

const Truck = mongoose.model('truck', trucksSchema);

module.exports = {
  Truck
};
